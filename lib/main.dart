import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Calculator'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String output = "0";
  MaterialColor color;
  String _output = "0";
  double num1 = 0.0;
  double num2 = 0.0;
  String operand = "";
  
  buttonPressed(String buttonText) {
    if (buttonText == "CLEAR") {
      _output = "0";
      num1 = 0.0;
      num2 = 0.0;
      operand = "";
    } else if (buttonText == "+" ||
        buttonText == "/" ||
        buttonText == "X" ||
        buttonText == "-" ||
        buttonText == "%" ||
        buttonText == "^" ||
        buttonText == "!") {
      num1 = double.parse(output);
      operand = buttonText;
      _output = "0";
    } else if (buttonText == '.') {
      if (_output.contains(".")) {
        print(". already present");
        return;
      } else {
        _output = _output + buttonText;
      }
    } else if (buttonText == "=") {
      if (operand == "!") {
        int fact = 1;
        if (num1 != num1.toInt()) {
          print('Not an Integer');
        }
        for (int i = 1; i <= num1; i++) {
          fact *= i;
        }

        _output = fact.toString();
      }

      if (operand == "+") {
        num2 = double.parse(output);
        _output = (num1 + num2).toString();
      }
      if (operand == "-") {
        num2 = double.parse(output);
        _output = (num1 - num2).toString();
      }
      if (operand == "/") {
        num2 = double.parse(output);
        _output = (num1 / num2).toString();
      }
      if (operand == "X") {
        num2 = double.parse(output);
        _output = (num1 * num2).toString();
      }
      if (operand == "%") {
        num2 = double.parse(output);
        _output = (num1 % num2).toString();
      }
      if (operand == "^") {
        num2 = double.parse(output);
        double res = 1;
        while (num2 != 0) {
          res *= num1;
          --num2;
        }
        _output = res.toString();
      }

      num1 = 0.0;
      num2 = 0.0;
      operand = "";
    } else {
      _output = _output + buttonText;
    }
    print(_output);
    setState(() {
      output = double.parse(_output).toStringAsFixed(2);
    });
  }

  Widget buildButton(String buttonText) {
    return Expanded(
      child: new OutlineButton(
        padding: EdgeInsets.all(24),
        child: new Text(
          buttonText,
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        onPressed: () => buttonPressed(buttonText),
      ),
    );
  }

  Widget buildColorButton(String buttonText) {
    color = Colors.blue;
    if (buttonText == "CLEAR") color = Colors.blueGrey;
    return Expanded(
      child: new RaisedButton(
        color: color,
        padding: EdgeInsets.all(24),
        child: new Text(
          buttonText,
          style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        onPressed: () => buttonPressed(buttonText),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Container(
        child: new Column(
          children: <Widget>[
            Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 12.0),
                child: new Text(
                  output,
                  style: TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
                )),
            new Expanded(child: new Divider()),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    buildButton("7"),
                    buildButton("8"),
                    buildButton("9"),
                    buildColorButton("/")
                  ],
                ),
                Row(
                  children: <Widget>[
                    buildButton("4"),
                    buildButton("5"),
                    buildButton("6"),
                    buildColorButton("X")
                  ],
                ),
                Row(
                  children: <Widget>[
                    buildButton("1"),
                    buildButton("2"),
                    buildButton("3"),
                    buildColorButton("-")
                  ],
                ),
                Row(
                  children: <Widget>[
                    buildButton("."),
                    buildButton("0"),
                    buildButton("00"),
                    buildColorButton("+")
                  ],
                ),
                Row(
                  children: <Widget>[
                    buildColorButton("!"),
                    buildColorButton("="),
                    buildColorButton("%"),
                    buildColorButton("^"),
                  ],
                ),
                Row(
                  children: <Widget>[
                    buildColorButton("CLEAR"),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
